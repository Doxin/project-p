import std.algorithm;
import std.math;
import std.string;
import std.stdio;
import std.conv;
import std.random;
import std.exception;
import noise;
import gaussian;
import image;

enum sampleDistance=0.001;
enum planetScale=1;
enum noiseScale=2;

struct Sample
{
    real elevation=0;
    real waterLevel=0;
    real waterSalinity=0;
    real latitude;
    real longitude;
}

real radians(real degrees)
{
    return degrees*(PI/180);
}

enum RenderType
{
    surface,
    heightMap,
    //waterSalinity,
}

class Planet
{
    Sample[] samples;
    OpenSimplexNoise[5] noiseGen;
    real[5] octaves=[0.53125,0.25,0.125,0.0625,0.03125];
    //real[5] octaves=[0.765625,0.125,0.0625,0.03125,0.015625];
    
    void log(string msg)
    {
        writeln(msg);
    }
    this()
    {
        log("generating initial samples");
        //noiseGen=new OpenSimplexNoise(uniform(long.min,long.max));
        for(int i=0;i<noiseGen.length;i+=1)
        {
            noiseGen[i]=new OpenSimplexNoise(uniform(long.min,long.max));
        }
        
        real circumference=PI_2;
        int latitudeSampleCount=((circumference/2.0L)/sampleDistance).to!int;
        real latitudeSampleDistance=180.0L/latitudeSampleCount;
        writeln(180,"/",latitudeSampleCount,"=",latitudeSampleDistance);
        log(format("%d latitude samples",latitudeSampleCount));
        //for(real latitude=-90+latitudeSampleDistance/2;latitude<90;latitude+=latitudeSampleDistance)
        real latitude=-90;
        size_t i;
        while(latitude<90)
        {
            real longitudinalCircumference=PI_2*cos(radians(latitude));
            int longitudeSampleCount=((longitudinalCircumference/2.0L)/sampleDistance).to!int;
            if(longitudeSampleCount>0)
            {
                real longitudeSampleDistance=360.0L/longitudeSampleCount;
                //for(real longitude=-180+longitudeSampleDistance/2;longitude<180;longitude+=longitudeSampleDistance)
                real longitude=-180;
                size_t j;
                while(longitude<180)
                {
                    Sample s;
                    s.latitude=latitude;
                    s.longitude=longitude;
                    real x=noiseScale*planetScale*cos(radians(latitude))*cos(radians(longitude));
                    real y=noiseScale*planetScale*cos(radians(latitude))*sin(radians(longitude));
                    real z=noiseScale*planetScale*sin(radians(latitude));
                    
                    s.elevation=0;
                    for(int n=0;n<noiseGen.length;n+=1)
                    {
                        real scale=pow(2,n+1)/2;
                        s.elevation+=(noiseGen[n].eval(x*scale,y*scale,z*scale)+1)/2*octaves[n];
                    }
                    enforce(s.elevation>=0);
                    
                    if(s.elevation<0.55)
                    {
                        s.waterLevel=0.55-s.elevation;
                        s.waterSalinity=1;
                    }
                    
                    samples~=s;
                    
                    j+=1;
                    longitude=(j*longitudeSampleDistance)-180.0L;
                }
            }
            
            i+=1;
            latitude=(i*latitudeSampleDistance)-90.0L;
        }
        log(format("generated %d samples total.",samples.length));
    }
    RGBAImage render(size_t size,RenderType renderType)
    {
        real circumference=PI_2;
        real sampleCount=((circumference/2.0L)/sampleDistance).to!int;
        
        real blur=(size*2)/sampleCount;
        //writeln("blur:",blur);
        
        log("rendering samples");
        auto img=RGBAImage(size*2,size);

        foreach(Sample s;samples)
        {
            int x=(((s.longitude/180.0L)+1)*(size-1)).to!int;
            int y=(((s.latitude/180.0L)+0.5)*(size-1)).to!int;
            real longitudinalCircumference=PI_2*cos(radians(s.latitude));
            int longitudeSampleCount=((longitudinalCircumference/2.0L)/sampleDistance).to!int;
            real longitudeSampleDistance=360.0L/longitudeSampleCount;
            real longitudeBlurDistance=longitudeSampleDistance/180.0L*(size-1);
            
            auto xkernel=gaussianKernel((longitudeBlurDistance+1)/2.0L);
            auto ykernel=gaussianKernel(blur/2.0L);
            
            /*auto xkernel=gaussianKernel(10);
            auto ykernel=gaussianKernel(10);*/
            
            int xhsize=floor(xkernel.length/2.0L).to!int;
            int yhsize=floor(ykernel.length/2.0L).to!int;
            for(int ox=-xhsize;ox<xhsize;ox+=1)
            for(int oy=-yhsize;oy<yhsize;oy+=1)
            {
                int rx=x+ox;
                int ry=y+oy;
                if(ry<0 || ry>=size)
                    continue;
                if(rx<0)
                    rx+=size*2;
                if(rx>=size*2)
                    rx-=size*2;
                
                real weight=xkernel[ox+xhsize]*ykernel[oy+yhsize];
                enforce(weight>0);
                enforce(weight<=1);
                real[4] color;
                switch(renderType)
                {
                    case RenderType.surface:
                        if(s.waterSalinity>0.5)
                            color=[0,0,(1-s.waterLevel)/2,1];
                        else
                            color=[0.2,s.elevation/2.0L,0.2,1];
                        break;
                    case RenderType.heightMap:
                        color=[s.elevation+s.waterLevel,s.elevation+s.waterLevel,s.elevation+s.waterLevel,1];
                        break;
                    default: throw new Exception("not implemented");
                }
                img.pixels[rx][ry][]+=color[]*weight;
            }
        }
        log("normalizing");
        
        for(size_t x=0;x<size*2;x+=1)
        for(size_t y=0;y<size;y+=1)
        {
            enforce(img.pixels[x][y][0]>=0);
            enforce(img.pixels[x][y][1]>=0);
            enforce(img.pixels[x][y][2]>=0);
            enforce(img.pixels[x][y][3]>=0);
            //writeln(img.pixels[x][y]);
            
            if(img.pixels[x][y][3]>0)
                {
                real w=img.pixels[x][y][3];
                //img.pixels[x][y][]/=w;
                img.pixels[x][y][0]/=w;
                img.pixels[x][y][1]/=w;
                img.pixels[x][y][2]/=w;
                img.pixels[x][y][3]/=w;
                }
            else
                img.pixels[x][y][]=[0,0,0,0];

            enforce(img.pixels[x][y][0]>=0);
            enforce(img.pixels[x][y][1]>=0);
            enforce(img.pixels[x][y][2]>=0);
            enforce(img.pixels[x][y][3]>=0);
        }
        
        return img;
    }
}
