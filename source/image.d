import std.conv;

struct RGBAImage
{
    real pixels[][][];
    size_t width;
    size_t height;
    this(size_t width,size_t height)
    {
        pixels=new real[][][](width,height,4);
        this.width=width;
        this.height=height;
        for(size_t x=0;x<width;x+=1)
            for(size_t y=0;y<height;y+=1)
                for(size_t c=0;c<4;c+=1)
                {
                    pixels[x][y][c]=0;
                }
    }
    void writePng(string filename)
    {
        import imageformats;
        auto realpixels=new ubyte[](this.width*this.height*4);
        size_t i=0;
        for(size_t y=0;y<height;y+=1)
            for(size_t x=0;x<width;x+=1)
                for(size_t c=0;c<4;c+=1)
                {
                    //writeln(pixels[x][y][c]);
                    auto val=pixels[x][y][c]*255;
                    /*if(val>255)
                        val=255;
                    if(val<0)
                        val=0;*/
                    realpixels[i]=val.to!ubyte;
                    i+=1;
                }
        write_png(filename,width,height,realpixels);
    }
}
