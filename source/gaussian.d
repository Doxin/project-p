import std.math;
import std.functional;
import std.conv;
import std.stdio;

private real gaussian(real x,real sig,real stdev)
{
    return exp(-pow(x-sig,2.0L)/(2.0L*pow(stdev,2.0L)));
}

real[] _gaussianKernel(real stdev)
{
    size_t size=ceil(6*stdev).to!size_t;
    if(size%2==0)//ensure the kernal is an odd-size for centering purposes.
        size+=1;
    size_t hsize=floor(size/2.0L).to!size_t;
    real kernel[]=new real[](size);
    real maxValue=0;
    for(size_t i=0;i<size;i+=1)
    {
        kernel[i]=gaussian(i,hsize,stdev);
        if(kernel[i]>maxValue)
            maxValue=kernel[i];
    }
    kernel[]/=maxValue;
    return kernel;
}

//alias gaussianKernel=memoize!_gaussianKernel;
alias gaussianKernel=_gaussianKernel;
